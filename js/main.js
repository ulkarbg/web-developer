function myMap() {
var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
};
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}


$(document).ready(function(){
	$('.menu-line').on("click",function(){
		$('.menu-rightbar').addClass('active-rightbar');
	});

	$('.fa-close').on("click",function(){
		$('.menu-rightbar').removeClass('active-rightbar');
	});

	var arrowUp = $('.arrow-icon');

	$(window).on('scroll', function(){

		if($(window).scrollTop() > 500) {
			arrowUp.fadeIn();
		} else {
			arrowUp.fadeOut();
		}
	});

	$('.arrow-up').on("click",function(){
		$('html,body').animate({scrollTop: 0},3000);
	});
});